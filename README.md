# G-Net Signal Service Proxy

Prototype implementation of a simple, low-bandwidth P2P data channel. Supports the exchange of [Interactive Connectivity Establishment (ICE)](https://en.wikipedia.org/wiki/Interactive_Connectivity_Establishment) signals, enabling ad-hoc peer-to-peer communication.
