from .authorization import AuthorizedKeyStore
from .protocol.keyring import PrivateKeyring


class SignalServiceConfiguration(object):

    class _SignalServiceConfigSingleton(object):

        def __init__(self, *args, **kwargs):
            self.keystore: AuthorizedKeyStore = kwargs.get("keystore", None)
            self.keyring: PrivateKeyring = kwargs.get("keyring", None)

    _INSTANCE: _SignalServiceConfigSingleton = None

    @staticmethod
    def keystore(self) -> AuthorizedKeyStore:
        return SignalServiceConfiguration().keystore
    
    @staticmethod
    def keyring(self) -> PrivateKeyring:
        return SignalServiceConfiguration().keyring

    def __new__(cls):
        if SignalServiceConfiguration._INSTANCE is None:
            SignalServiceConfiguration._INSTANCE = SignalServiceConfiguration._SignalServiceConfigSingleton()
        return SignalServiceConfiguration._INSTANCE
