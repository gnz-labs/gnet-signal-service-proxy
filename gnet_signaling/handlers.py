import base64
import binascii
import logging
import socketserver
from dataclasses import dataclass
from .authorization import AuthorizedKeyStore
from .authorization.errors import PeerNotFoundException
from .protocol.datagram import DatagramHeader, SealedDatagram
from .protocol.error_codes import ProtocolError
from .protocol.encryption import SignalEncryption
from .protocol.keyring import PrivateKeyring, PublicKeyring
from .protocol import signals
from .service_configuration import SignalServiceConfiguration


@dataclass
class SignalingProtocolHandler(object):

    keyring: PrivateKeyring
    keystore: AuthorizedKeyStore

    def unwrap_signal(self, wrapped_signal: bytes) -> signals.Signal:
        sealed_datagram = SealedDatagram.unpack(wrapped_signal)
        unpacked_header = DatagramHeader.unpack(sealed_datagram.header)
        peer_public_key = self.keystore.get_public_key(id=unpacked_header.peer_id)
        return signals.parse(
            signals.SignalType(unpacked_header.content_type),
            SignalEncryption(self.keyring).decrypt(
                PublicKeyring(None, peer_public_key), 
                sealed_datagram
            )
        )        


class SignalingServiceHandler(socketserver.StreamRequestHandler):

    def _log_error(self, message):        
        logging.error(f"[{self.client_address}] {message}")

    def _respond_with_status(self, status_code: ProtocolError):
        # Boolean status code to strip out oracles
        self.rfile.write(
            base64.b64encode(
                int(status_code & ProtocolError.InvalidMessage).to_bytes(1, "big")
            ) + b"\n"
        )        
        self.rfile.flush()

    def _handle_invite(self, signal: signals.SessionInvitation):
        pass

    def _handle_advertise_route(self, signal: signals.RouteAdvertisement):
        pass   

    def _handle_signal(self, signal: signals.Signal):
        if signal.signal_type == signals.SignalType.SessionInvitation:
            self._handle_invite(signal)
        elif signal.signal_type == signals.SignalType.RouteAdvertisement:
            self._handle_advertise_route(signal)
            
    def setup(self) -> None:
        self.protocol_handler = SignalingProtocolHandler(
            SignalServiceConfiguration.keyring(),
            SignalServiceConfiguration.keystore()
        )
        return super().setup()

    def handle(self):
        signal: signals.Signal = None
        status_code = ProtocolError.InvalidMessage        
        try:
            signal = self.protocol_handler.unwrap_signal(base64.b64decode(self.rfile.readline().strip()))
            status_code = ProtocolError.NoError
        except binascii.Error:
            status_code &= ProtocolError.IncorrectPadding
            self._log_error(f"incorrect padding")
        except PeerNotFoundException as e:
            status_code &= ProtocolError.PeerNotFound
            self._log_error(str(e))
        if signal is not None and status_code == ProtocolError.NoError:
            self._handle_signal(signal)
        else:
            self._log_error(f"signal handling failed with status code: {status_code}")
        self._respond_with_status(status_code)
