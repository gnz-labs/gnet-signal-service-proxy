import os
from collections import namedtuple
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import x25519
from cryptography.hazmat.primitives.ciphers.aead import ChaCha20Poly1305
from cryptography.hazmat.primitives.hashes import SHA3_256
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from datetime import datetime
from .datagram import Datagram, DatagramHeader, UnsealedDatagram, SealedDatagram
from .keyring import PrivateKeyring, PublicKeyring
from .signals import Signal


class SignalEncryption(object):

    def __init__(self, service_keyring: PrivateKeyring):
        self._keyring = service_keyring

    @staticmethod
    def _get_cipher(private_key: x25519.X25519PrivateKey, public_key: x25519.X25519PublicKey) -> ChaCha20Poly1305:
        return ChaCha20Poly1305(HKDF(
            algorithm=SHA3_256(),
            length=32,
            salt=None,
            info=None
        ).derive(private_key.exchange(public_key)))
    
    def encrypt_bytes(self, private_key: x25519.X25519PrivateKey, public_key: x25519.X25519PublicKey, data: bytes, aad: bytes = None) -> bytes:
        nonce = os.urandom(12)
        cipher = self._get_cipher(private_key, public_key)
        return nonce + cipher.encrypt(nonce, data, aad)

    def decrypt_bytes(self, private_key: x25519.X25519PrivateKey, public_key: x25519.X25519PublicKey, ciphertext: bytes, aad: bytes = None) -> bytes:
        cipher = self._get_cipher(private_key, public_key)
        return cipher.decrypt(ciphertext[:12], ciphertext[12:], aad)

    def encrypt(self, peer_id: bytes, source: str, destination: str, destination_keys: PublicKeyring, signal: Signal) -> SealedDatagram:
        packed_datagram = Datagram(source, destination, datetime.utcnow().timestamp(), signal.pack()).pack()
        datagram_header = DatagramHeader(
            peer_id=peer_id,
            content_type=int(signal.signal_type()),
            content_length=len(packed_datagram)
        )
        return SealedDatagram(header=datagram_header.pack(), body=self.encrypt_bytes(self._keyring.encryption_key, destination_keys.encryption_key, packed_datagram, datagram_header))

    def decrypt(self, sender_keys: PublicKeyring, sealed_datagram: SealedDatagram) -> UnsealedDatagram:
        packed_datagram = self.decrypt_bytes(self._keyring.encryption_key, sender_keys.encryption_key, sealed_datagram.body, sealed_datagram.header)
        return UnsealedDatagram(DatagramHeader.unpack(sealed_datagram.header), Datagram.unpack(packed_datagram))
