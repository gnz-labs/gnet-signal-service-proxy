import json
from dataclasses import dataclass
from .header import DatagramHeader
from .unsealed_datagram import UnsealedDatagram
from .sealed_datagram import SealedDatagram


@dataclass
class Datagram(object):

    source: str
    destination: str
    timestamp: int 
    content: bytes

    def pack(self) -> bytes:
        bytes(json.dumps(self.__dict__), 'utf-8')

    @staticmethod
    def unpack(packed_datagram: bytes):
        return json.loads(str(packed_datagram, 'utf-8'))


__all__ = [Datagram.__name__, DatagramHeader.__name__, UnsealedDatagram.__name__, SealedDatagram.__name__]
