from dataclasses import dataclass


PEER_ID_SIZE = 32
CONTENT_LENGTH_SIZE = 4
CONTENT_TYPE_SIZE = 2
HEADER_SIZE = PEER_ID_SIZE + CONTENT_TYPE_SIZE + CONTENT_LENGTH_SIZE


@dataclass
class DatagramHeader(object):

    peer_id: bytes
    content_type: int
    content_length: int

    def pack(self) -> bytes:
        return self.peer_id + \
            self.content_type.to_bytes(CONTENT_TYPE_SIZE, 'big') + \
            self.content_length.to_bytes(CONTENT_LENGTH_SIZE, 'big')
    
    @staticmethod
    def unpack(packed_header: bytes):
        if len(packed_header) != HEADER_SIZE:
            raise ValueError(f"Invalid header length ({len(packed_header)}). Expected {HEADER_SIZE}.")
        return DatagramHeader(
            peer_id=packed_header[:PEER_ID_SIZE],
            content_type=int.from_bytes(packed_header[PEER_ID_SIZE:PEER_ID_SIZE+CONTENT_TYPE_SIZE], "big"),
            content_length=int.from_bytes(packed_header[HEADER_SIZE-CONTENT_LENGTH_SIZE:HEADER_SIZE], "big")
        )
