from dataclasses import dataclass
from .header import HEADER_SIZE

SIGNATURE_SIZE = 64

@dataclass
class SealedDatagram(object):

    header: bytes
    body: bytes

    def pack(self) -> bytes:
        return self.header + \
               self.body

    @staticmethod
    def unpack(sealed_datagram: bytes):
        return SealedDatagram(
            header=sealed_datagram[:HEADER_SIZE],
            body=sealed_datagram[HEADER_SIZE:]
        )
