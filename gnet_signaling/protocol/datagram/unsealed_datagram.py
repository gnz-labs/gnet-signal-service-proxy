from dataclasses import dataclass
from . import Datagram
from .header import DatagramHeader


@dataclass
class UnsealedDatagram(object):

    header: DatagramHeader
    body: Datagram