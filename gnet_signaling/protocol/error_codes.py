from enum import IntFlag, auto


class ProtocolError(IntFlag):

    NoError = auto()
    InvalidMessage = auto()
    IncorrectPadding = auto()
    PeerNotFound = auto()
    UnsupportedSignal = auto()
