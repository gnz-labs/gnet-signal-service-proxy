from cryptography.hazmat.primitives.asymmetric import ed25519, x25519
from dataclasses import dataclass
from .public_keyring import PublicKeyring


@dataclass
class PrivateKeyring(object):

    signing_key: ed25519.Ed25519PrivateKey
    encryption_key: x25519.X25519PrivateKey

    @property
    def public_keyring(self) -> PublicKeyring:
        return PublicKeyring(
            signing_key=self.signing_key.public_key(),
            encryption_key=self.encryption_key.public_key()
        )
