from cryptography.hazmat.primitives.asymmetric import ed25519, x25519
from dataclasses import dataclass


@dataclass
class PublicKeyring(object):

    signing_key: ed25519.Ed25519PublicKey
    encryption_key: x25519.X25519PublicKey
