from dataclasses import dataclass
from .device_auth import DeviceAuthSignal
from .signal import Signal
from .signal_type import SignalType


@dataclass
class SessionInvitation(object):

    @staticmethod
    def from_json(content: bytes):
        raise NotImplementedError()
    

@dataclass
class RouteAdvertisement(object):

    @staticmethod
    def from_json(content: bytes):
        raise NotImplementedError()


def parse(content_type: SignalType, content: bytes) -> Signal:
    if content_type == SignalType.SessionInvitation:
        return SessionInvitation.from_json(content)
    elif content_type == SignalType.RouteAdvertisement:
        return RouteAdvertisement.from_json(content)
    return None


__all__ = [
    "parse", 
    RouteAdvertisement.__name__,
    SessionInvitation.__name__, 
    DeviceAuthSignal.__name__, 
    Signal.__name__, 
    SignalType.__name__
]
