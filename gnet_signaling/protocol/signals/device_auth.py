from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PublicKey
from datetime import datetime
from .signal import Signal


class DeviceAuthSignal(Signal):

    def __init__(self, device_id: str, signal_service_address: str, public_key: Ed25519PublicKey, **kwargs):
        self.device_id: str = device_id
        self.signal_service_address: str = signal_service_address
        self.public_key: Ed25519PublicKey = public_key
        self.timestamp: datetime = kwargs.get("timestamp", datetime.now())

    def to_json(self) -> bytes:
        raise NotImplementedError()
    
    @staticmethod
    def from_json(auth_request: bytes):
        raise NotImplementedError()
