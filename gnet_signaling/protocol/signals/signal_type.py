from enum import IntEnum


class SignalType(IntEnum):

    Unknown = 0
    SessionInvitation = 1
    RouteAdvertisement = 2
