from .signal_type import SignalType

class Signal(object):

    def __init__(self, signal_type: SignalType, *args, **kwargs):
        self.signal_type = signal_type

    def pack(self) -> bytes:
        raise NotImplementedError()
