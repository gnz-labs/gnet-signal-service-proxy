class PeerInfo(object):

    def __init__(self, **kwargs):
        self.id = kwargs.get("id", "")
        self.public_key = kwargs.get("public_key", "")
        self.signaling_address = kwargs.get("signaling_address", "")
