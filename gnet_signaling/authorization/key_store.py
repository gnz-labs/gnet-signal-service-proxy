import base64
from cryptography.hazmat.primitives.asymmetric.x25519 import X25519PublicKey
from ._backend import AuthorizationBackend
from .errors import PeerNotFoundException


class AuthorizedKeyStore(object):

    def __init__(self, backend: AuthorizationBackend):
        self._backend = backend

    def get_public_key(self, **kwargs) -> X25519PublicKey:
        public_key_string = None
        if kwargs.get("id", ""):
            public_key_string = self._backend.find_by_id(kwargs.get("id"))
        elif kwargs.get("signaling_address", ""):
            public_key_string = self._backend.find_by_address(kwargs.get("signaling_address"))
        if public_key_string is not None:
            return X25519PublicKey.from_public_bytes(base64.b64decode(public_key_string))
        raise PeerNotFoundException(str(kwargs))
