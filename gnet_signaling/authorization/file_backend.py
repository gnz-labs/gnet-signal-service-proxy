import json
from pathlib import Path
from typing import Union
from threading import Lock
from ._backend import AuthorizationBackend
from .peer_info import PeerInfo


class FileBackend(AuthorizationBackend):

    def __init__(self, file_path: Union[str, Path]):
        self._file_path = file_path if type(file_path) is Path else Path(file_path)
        self._lock = Lock()

    def authorize(self, peer_info: PeerInfo):
        self._lock.acquire()
        with open(self._file_path, "r") as infile:
            authorized_peers = json.load(infile)
        authorized_peers.append(peer_info.__dict__)
        with open(self._file_path, "w") as outfile:
            json.dump(outfile, indent=4, sort_keys=True)
        self._lock.release()
        return super().authorize(peer_info)

    def prepare(self):
        self._lock.acquire()
        with open(self._file_path, "r") as infile:
            authorized_peers = json.load(infile)
        self._lock.release()
        for peer in authorized_peers:
            super().authorize(PeerInfo(**peer))
