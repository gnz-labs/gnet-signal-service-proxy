from ._cache import AuthorizationCache
from .errors import PeerNotFoundException
from .peer_info import PeerInfo


class AuthorizationBackend(object):

    def _query_cache(self, query, args) -> PeerInfo:
        peer_info = None
        with AuthorizationCache() as cursor:
            cursor.execute(query, args)
            peer_info = cursor.fetchone()
        if peer_info is None:
            raise PeerNotFoundException(args)
        return PeerInfo(**peer_info)
    
    def authorize(self, peer_info: PeerInfo):
        for required_key in ["id", "public_key", "signaling_address"]:
            if not getattr(peer_info, required_key):
                raise ValueError(peer_info.__dict__)
        with AuthorizationCache() as cursor:
            cursor.execute(
                "INSERT INTO authorized_peers VALUES(?,?,?)", 
                (peer_info.id, peer_info.public_key, peer_info.signaling_address)
            )                   

    def find_by_id(self, peer_id: str) -> PeerInfo:
        return self._query_cache("SELECT * FROM authorized_peers WHERE id=? LIMIT 1", (peer_id,))

    def find_by_public_key(self, public_key: str) -> PeerInfo:
        return self._query_cache("SELECT * FROM authorized_peers WHERE public_key=? LIMIT 1", (public_key,))
    
    def find_by_address(self, signaling_address: str) -> PeerInfo:
        return self._query_cache("SELECT * FROM authorized_peers WHERE signaling_address=? LIMIT 1", (signaling_address,))

    def find_peer(self, **kwargs) -> PeerInfo:
        if kwargs.get("id", ""):
            return self.find_by_id(kwargs.get("id"))
        elif kwargs.get("public_key", ""):
            return self.find_by_public_key(kwargs.get("public_key"))
        elif kwargs.get("signaling_address", ""):
            return self.find_by_address(kwargs.get("signaling_address"))
        else:
            raise PeerNotFoundException(str(kwargs))
