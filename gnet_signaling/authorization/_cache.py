import sqlite3
from threading import Lock


class AuthorizationCache(object):

    class _AuthorizationCache(object):

        def __init__(self):
            self.lock = Lock()
            self.database = sqlite3.connect(":memory:")

        def _prepare(self):
            self.lock.acquire()
            cursor = AuthorizationCache.cursor()
            cursor.execute("CREATE TABLE authorized_peers(UNIQUE(id), UNIQUE(public_key), UNIQUE(signaling_address))")
            cursor.close()
            self.database.commit()
            self.lock.release()

    _INSTANCE: _AuthorizationCache = None

    def __new__(cls):
        if AuthorizationCache._INSTANCE is None:
            AuthorizationCache._INSTANCE = AuthorizationCache._AuthorizationCache()
            AuthorizationCache._INSTANCE._prepare()
        return AuthorizationCache._INSTANCE
    
    def __init__(self):
        self._cursor: sqlite3.Cursor = None

    def __enter__(self):
        AuthorizationCache._INSTANCE.lock.acquire()
        self._cursor = AuthorizationCache._INSTANCE.database.cursor()
        return self._cursor
    
    def __exit__(self, *args):
        if self._cursor is not None:
            self._cursor.close()
        self._cursor = None
        AuthorizationCache._INSTANCE.database.commit()
        AuthorizationCache._INSTANCE.lock.release()
