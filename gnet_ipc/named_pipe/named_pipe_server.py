import socket
import socketserver
import win32pipe
from typing import Callable, Any

class NamedPipeServer(socketserver.BaseServer):

    def __init__(self, server_address: socketserver._Address, RequestHandlerClass: Callable[[Any, socketserver._RetAddress, socketserver.Self], socketserver.BaseRequestHandler]) -> None:

        super().__init__(server_address, RequestHandlerClass)

    def fileno(self) -> int:
        return super().fileno()
    
    def handle_request(self) -> None:
        return super().handle_request()
    
    def handle_timeout(self) -> None:
        return super().handle_timeout()

    def handle_error(self, request: socketserver._RequestType, client_address: socketserver._RetAddress) -> None:
        return super().handle_error(request, client_address)
    