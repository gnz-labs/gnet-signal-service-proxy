import pathlib
import pywintypes
import socket
import sys
import win32api
import win32file
import win32pipe
from typing import Union
from .options import PipeOptions



class NamedPipeSocket(socket.socket):

    def __init__(self, _options: PipeOptions = PipeOptions(), *args, **kwargs):
        self._options = _options 
        self._is_server: bool = kwargs.get("_is_server", False)
        self._pipe_handle: pywintypes.HANDLE = kwargs.get("_pipe_handle", None)

    @property
    def pipe_handle(self) -> pywintypes.HANDLE:
        if self._pipe_handle is win32file.INVALID_HANDLE_VALUE:
            raise OSError(win32api.GetLastError())
        elif self._pipe_handle is None:
            raise socket.error("No pipe")
        return self._pipe_handle
    
    @property.setter("pipe_handle")
    def pipe_handle(self, handle: pywintypes.HANDLE) -> None:
        if self._pipe_handle is not None:
            raise socket.error("Handle already created")
        elif handle is win32file.INVALID_HANDLE_VALUE:
            raise OSError(f"Invalid handle: {win32api.GetLastError()}")
        elif handle is None:
            raise ValueError("Null handle")
        sys.audit(f"{self.__class__.__name__}.pipe_handle", handle)
        self._pipe_handle = handle
  
    def accept(self) -> tuple[socket.socket, socket._RetAddress]:
        return super().accept()
    
    def bind(self, address: str) -> None:
        self._options.pipe_name = address or self._options.pipe_name
        sys.audit(f"{self.__class__.__name__}.bind", self, self._options.pipe_name)
        self.pipe_handle = win32pipe.CreateNamedPipe(
            pipeName=self._options.pipe_name,
            openMode=self._options.open_mode,
            pipeMode=self._options.pipe_mode,
            nMaxInstances=self._options.max_instances,
            nInBufferSize=self._options.input_buffer_size,
            nOutBufferSize=self._options.output_buffer_size,
            nDefaultTimeOut=self._options.default_timeout_ms,
        )
        self._is_server = True

    def close(self) -> None:
        if self._is_server:
            win32file.FlushFileBuffers(self.pipe_handle)
            win32pipe.DisconnectNamedPipe(self.pipe_handle)
        else:
            win32file.CloseHandle(self.pipe_handle)
        self._pipe_handle = None
    
    def connect(self, address: str) -> None:        
        if not self.connect_ex(address):
            raise OSError(f"Connection error ({win32api.GetLastError()})")
    
    def connect_ex(self, address: str) -> int:
        self._options.pipe_name = address or self._options.pipe_name
        sys.audit(f"{self.__class__.__name__}.connect", self, self._options.pipe_name)
        self.pipe_handle = win32file.CreateFile(
            self._options.pipe_name, 
            win32file.GENERIC_READ | win32file.GENERIC_WRITE,
            0,
            None,
            win32file.OPEN_EXISTING,
            0,
            None
        )
        return win32pipe.SetNamedPipeHandleState(self.pipe_handle, self._options.pipe_mode, None, None)
    
    def detach(self) -> int:
        handle = self.pipe_handle
        self._pipe_handle = None
        return handle
    
    def dup(self) -> socket.Self:
        return self.__class__(self.__dict__)
    
    def fileno(self) -> int:
        return self._pipe_handle
    
    def get_inheritable(self) -> bool:
        return False
    
    def getpeername(self) -> socket._RetAddress:
        return super().getpeername()
    
    def getsockname(self) -> socket._RetAddress:
        return super().getsockname()
    
    def getblocking(self) -> bool:
        return self.gettimeout() == 0
    
    def gettimeout(self) -> float | None:
        return super().gettimeout()
    
    def listen(self, __backlog: int = ...) -> None:
        return super().listen(__backlog)
    
    def recv(self, __bufsize: int, __flags: int = ...) -> bytes:
        return super().recv(__bufsize, __flags)
    
    def recvfrom(self, __bufsize: int, __flags: int = ...) -> tuple[bytes, socket._RetAddress]:
        return super().recvfrom(__bufsize, __flags)
    
    def recv_into(self, buffer: socket.WriteableBuffer, nbytes: int = ..., flags: int = ...) -> int:
        return super().recv_into(buffer, nbytes, flags)
    
    def recvfrom_into(self, buffer: socket.WriteableBuffer, nbytes: int = ..., flags: int = ...) -> tuple[int, socket._RetAddress]:
        return super().recvfrom_into(buffer, nbytes, flags)
    
    def send(self, __data: socket.ReadableBuffer, __flags: int = ...) -> int:
        return super().send(__data, __flags)
    
    def sendall(self, __data: socket.ReadableBuffer, __flags: int = ...) -> None:
        return super().sendall(__data, __flags)
    
    def sendfile(self, file: socket._SendableFile, offset: int = 0, count: int | None = None) -> int:
        return super().sendfile(file, offset, count)
    
    def set_inheritable(self, inheritable: bool) -> None:
        return super().set_inheritable(inheritable)
    
    def setblocking(self, __flag: bool) -> None:
        return super().setblocking(__flag)
    
    def settimeout(self, __value: float | None) -> None:
        return super().settimeout(__value)
    
    def shutdown(self, __how: int) -> None:
        return super().shutdown(__how)
    
    def share(self, __process_id: int) -> bytes:
        return super().share(__process_id)