import enum
import pywintypes
import win32file
import win32pipe
from dataclasses import dataclass
from typing import Union


UnlimitedInstances = 255


class OpenMode(enum.IntFlag):

    Inbound = win32pipe.PIPE_ACCESS_INBOUND
    Outbound = win32pipe.PIPE_ACCESS_OUTBOUND
    Duplex = win32pipe.PIPE_ACCESS_DUPLEX
    FirstPipe = win32pipe.FILE_FLAG_FIRST_PIPE_INSTANCE
    WriteThrough = win32file.FILE_FLAG_WRITE_THROUGH
    Overlapped = win32file.FILE_FLAG_OVERLAPPED
    WriteDac = 0x00040000
    WriteOwner = 0x00010000
    AccessSystemSecurity = 0x01000000


class RemoteClientMode(enum.IntFlag):

    Accept = 0
    Reject = 8


class WaitMode(enum.IntFlag):

    Blocking = 0
    NonBlocking = 1

class ReadDataFormat(enum.IntFlag):

    Byte = 0
    Message = 2

class DataFormat(enum.IntFlag):

    Byte = 0
    Message = 4


@dataclass
class PipeOptions(object):

    pipe_name: str = ""
    open_mode: OpenMode = OpenMode.Duplex
    pipe_mode: Union[RemoteClientMode, WaitMode, ReadDataFormat, DataFormat] = DataFormat.Byte & WaitMode.Blocking
    max_instances: Union[int, UnlimitedInstances] = 0
    input_buffer_size: int = 4096
    output_buffer_size: int = 4096
    default_timeout_ms: int = 50
    security_attributes: pywintypes.SECURITY_ATTRIBUTES = None
